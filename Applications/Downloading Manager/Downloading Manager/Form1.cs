﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace Downloading_Manager
{
    public partial class Form1 : Form
    {
        private int presentIndex = 0;
        private WebClient client;
        private bool downloading = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != String.Empty && textBox1.Text.Trim() != string.Empty)
                listBox1.Items.Add(textBox1.Text);
            textBox1.Text = String.Empty;
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            int indeks = listBox1.SelectedIndex;
            if (indeks == -1)
                return;
            listBox1.Items.RemoveAt(indeks);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                textBox2.Text = folderBrowserDialog1.SelectedPath;
        }

        void klient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label3.Text = "Downloaded: " + (Math.Round(e.BytesReceived /
            (double)1024, 2)).ToString() + " kB z " +
            (Math.Round(e.TotalBytesToReceive / (double)1024, 2)).ToString() + " kB";
            label4.Text = e.ProgressPercentage.ToString() + "%";
        }

        void klient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (presentIndex < listBox1.Items.Count)
            {
                Uri url = new Uri(listBox1.Items[presentIndex].ToString());
                DownloadAsynchronyously(url);
            }
            else
            {
                if (e.Cancelled || e.Error != null)
                    MessageBox.Show("Downloading aborted or an error occurred.(" + e.Error.Message + ")", "Information");
                else
                    MessageBox.Show("Downloading Completed!", "Information");
                downloading = false;
            }
        }


        private void DownloadAsynchronyously(Uri url)
        {
            listBox1.SelectedIndex = presentIndex;
            listBox1.Focus();
            downloading = true;
            client = new WebClient();
            client.DownloadFileCompleted += new
            AsyncCompletedEventHandler(klient_DownloadFileCompleted);
            client.DownloadProgressChanged += new
            DownloadProgressChangedEventHandler(klient_DownloadProgressChanged);
            string path = url.ToString();
            string fileName = textBox2.Text + "\\" +
            path.Substring(path.LastIndexOf("/") + 1, path.Length - path.LastIndexOf("/") - 1);
            try
            {
                FileInfo plik = new FileInfo(fileName);
                if (plik.Exists == false)
                {
                    client.DownloadFileAsync(url, textBox2.Text + "\\" +
                    path.Substring(path.LastIndexOf("/") + 1,
                    path.Length - path.LastIndexOf("/") - 1));
                    presentIndex++;
                }
                else
                {
                    MessageBox.Show("File name: " + fileName + " exisits");
                    downloading = false;
                    if (++presentIndex < listBox1.Items.Count)
                    {
                        Uri nowy = new
                        Uri(listBox1.Items[presentIndex].ToString());
                        DownloadAsynchronyously(nowy);
                    }
                }
            }
            catch
            {
                MessageBox.Show("File cannot be downloaded " + fileName);
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            presentIndex = 0;
            Uri url = new Uri(listBox1.Items[presentIndex].ToString());
            DownloadAsynchronyously(url);
        }

        private void stopDownloading()
        {
            client.CancelAsync();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to stop downloading?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                stopDownloading();
                progressBar1.Value = 0;
                label3.Text = "Downloaded: ";
                label4.Text = "0%";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (downloading)
                if (MessageBox.Show("Download in progress. Do you really want to exit?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    e.Cancel = true;
        }
    }
}
