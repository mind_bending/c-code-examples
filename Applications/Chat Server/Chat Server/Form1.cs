﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.IO;

namespace Chat_Server
{
    public partial class Form1 : Form
    {
        private TcpListener server;
        private TcpClient client;
        private ArrayList clientsList;
        private ArrayList namesClients;
        private bool isServerActive = false;

        public Form1()
        {
            InitializeComponent();

            clientsList = new ArrayList();
            namesClients = new ArrayList();
            isServerActive = false;
            webBrowserChat.Document.Write("<html><head><style>body,table{ font-size: 10pt; font-family: Verdana; margin: 3px 3px 3px 3px;font-color: black;}</style></head><body width=\"" + (webBrowserChat.ClientSize.Width - 20).ToString() + "\">");
        }

        delegate void SetTextCallBack(ListBox list, string text);
        private void SetText(ListBox list, string text)
        {
            if (list.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { list, text });
            }
            else
            {
                list.Items.Add(text);
            }
        }
        delegate void SetTextHTMLCallBack(string text);
        private void SetTextHTML(string text)
        {
            if (webBrowserChat.InvokeRequired)
            {
                SetTextHTMLCallBack f = new SetTextHTMLCallBack(SetTextHTML);
                this.Invoke(f, new object[] { text });
            }
            else
            {
                this.webBrowserChat.Document.Write(text);
            }
        }
        delegate void SetScrollCallBack();
        private void SetScroll()
        {
            if (webBrowserChat.InvokeRequired)
            {
                SetScrollCallBack f = new SetScrollCallBack(SetScroll);
                this.Invoke(f);
            }
            else
            {
                this.webBrowserChat.Document.Window.ScrollTo(1, int.MaxValue);
            }
        }
        delegate void RemoveTextCallBack(int i);
        private void RemoveText(int i)
        {
            if (listBoxUsers.InvokeRequired)
            {
                RemoveTextCallBack f = new RemoveTextCallBack(RemoveText);
                this.Invoke(f, new object[] { i });
            }
            else
            {
                listBoxUsers.Items.RemoveAt(i);
            }
        }

        private void AddText(string who, string message)
        {
            SetTextHTML("<table><tr><td width=\"10%\"><b>[" + who + "]:</b></td>");
            SetTextHTML("<td colspan=2>" + message + "</td></tr></table>");
            SetScroll();
        }

        private void SendUdpMessage(string message)
        {
            foreach (string user in listBoxUsers.Items)
                using (UdpClient klientUDP = new UdpClient(user, 43210))
                {
                    byte[] buffer = Encoding.UTF8.GetBytes(message);
                    klientUDP.Send(buffer, buffer.Length);
                }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorkerMainLoop_DoWork(object sender, DoWorkEventArgs e)
        {
            //
            try
            {
                server.Start();
                SetText(listBoxServer, "Server is waiting for connections ...");
                while (true)
                {
                    client = server.AcceptTcpClient();
                    SetText(listBoxServer, "Client connected");
                    NetworkStream ns = client.GetStream();
                    BinaryReader read = new BinaryReader(ns);
                    string data = read.ReadString();
                    string[] cmd = data.Split(new char[] { ':' });
                    if (cmd[1] == "HI")
                    {
                        BinaryWriter write = new BinaryWriter(ns);
                        if (namesClients.BinarySearch(cmd[0]) > -1)
                        {
                            write.Write("ERROR:User with such name already exists.");
                        }
                        else
                        {
                            write.Write("HI");
                            BackgroundWorker clientThread = new BackgroundWorker();
                            clientThread.WorkerSupportsCancellation = true;
                            clientThread.DoWork += new DoWorkEventHandler(clientThread_DoWork);
                            namesClients.Add(cmd[0]);
                            clientsList.Add(clientThread);
                            clientThread.RunWorkerAsync();
                            SendUdpMessage("administrator:SAY:User " + cmd[0] + " joined the conversation.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Client did not authorize itself.");
                        isServerActive = false;
                        client.Close();
                    }
                }
            }
            catch
            {
                isServerActive = false;
                server.Stop();
                SetText(listBoxServer, "Connection aborted/closed.");
            }
            //
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (!isServerActive)
                try
                {
                    server = new TcpListener(IPAddress.Parse(comboBoxIpAddress.Text), (int)numericUpDownPort.Value);
                    isServerActive = true;
                    backgroundWorkerMainLoop.RunWorkerAsync();
                    //isServerActive = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Server initialization error (" + ex.Message + ")");
                }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (isServerActive)
            {
                SendUdpMessage("administrator:SAY:Server is going down...");
                if (client != null) client.Close();
                server.Stop();
                listBoxServer.Items.Add("Server was closed.");
                listBoxUsers.Items.Clear();
                namesClients.Clear();
                clientsList.Clear();
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text != String.Empty && textBoxMessage.Text.Trim() != String.Empty)
            {
                AddText("administrator", textBoxMessage.Text);
                if (isServerActive) SendUdpMessage("administrator:SAY:" + textBoxMessage.Text);
            }
        }

        private void textBoxMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) buttonStop_Click(this, null);
        }

        void clientThread_DoWork(object sender, DoWorkEventArgs e)
        {
            IPEndPoint IP = (IPEndPoint)client.Client.RemoteEndPoint;
            SetText(listBoxUsers, IP.Address.ToString());
            SetText(listBoxServer, "Client [" + IP.Address.ToString() + "]authorized");
            NetworkStream ns = client.GetStream();
            BinaryReader read = new BinaryReader(ns);
            string[] cmd = null;
            BackgroundWorker bw = (BackgroundWorker)sender;
            try
            {
                while ((cmd = read.ReadString().Split(new char[] { ':' }))[1] != "BYE" && bw.CancellationPending == false)
                {
                    string message = null;
                    if (cmd.Length > 2)
                    {
                        message = cmd[2];
                        for (int i = 3; i < cmd.Length; i++)
                            message += ":" + cmd[i];
                    }
                    switch (cmd[1])
                    {
                        case "SAY":
                            AddText(cmd[0], message);
                            SendUdpMessage(cmd[0] + ":" + cmd[1] + ":" + message);
                            break;
                    }
                }
                SetText(listBoxServer, "User [" + cmd[0] + "] left the server");
                for (int i = 0; i < listBoxUsers.Items.Count; i++)
                    if (IP.Address.ToString() == listBoxUsers.Items[i].ToString())
                    {
                        RemoveText(i);
                        namesClients.RemoveAt(i);
                        clientsList.RemoveAt(i);
                    }
                SendUdpMessage("administrator:SAY:User " + cmd[0] + " left chat");
                read.Close();
                ns.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {

            {
                int index = listBoxUsers.SelectedIndex;
                using (UdpClient clientUdp = new UdpClient(listBoxUsers.Items[index].ToString(), 26000))
                {
                    byte[] buff = Encoding.UTF8.GetBytes("administrator:SAY:You were disconnected.");
                    clientUdp.Send(buff, buff.Length);
                    byte[] bufor2 = Encoding.UTF8.GetBytes("administrator:BYE:empty");
                    clientUdp.Send(bufor2, bufor2.Length);
                }
                listBoxServer.Items.Add("Client [" +
                listBoxUsers.Items[index].ToString() + "] disconnected");
                ((BackgroundWorker)clientsList[index]).CancelAsync();
                SendUdpMessage("administrator:SAY:User " +
                listBoxUsers.Items[index].ToString() + " was disconnected");
                listBoxUsers.Items.RemoveAt(index);
                clientsList.RemoveAt(index);
                namesClients.RemoveAt(index);
            }

        }

        private void backgroundWorkerMainLoop_DoWork_1(object sender, DoWorkEventArgs e)
        {
            try
            {
                server.Start();
                SetText(listBoxServer, "Server is waiting for incoming connections ...");
                while (true)
                {
                    client = server.AcceptTcpClient();
                    SetText(listBoxServer, "Client connected");
                    NetworkStream ns = client.GetStream();
                    BinaryReader read = new BinaryReader(ns);
                    string data = read.ReadString();
                    string[] cmd = data.Split(new char[] { ':' });
                    if (cmd[1] == "HI")
                    {
                        BinaryWriter write = new BinaryWriter(ns);
                        if (namesClients.BinarySearch(cmd[0]) > -1)
                        {
                            write.Write("ERROR:User with such name already exists.");
                        }
                        else
                        {
                            write.Write("HI");
                            BackgroundWorker clientThread = new BackgroundWorker();
                            clientThread.WorkerSupportsCancellation = true;
                            clientThread.DoWork += new DoWorkEventHandler(clientThread_DoWork);
                            namesClients.Add(cmd[0]);
                            clientsList.Add(clientThread);
                            clientThread.RunWorkerAsync(); SendUdpMessage("administrator:SAY:User " + cmd[0] + " joined conversation.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Client did not aouthorize itself.");
                        isServerActive = false;
                        client.Close();
                    }
                }
            }
            catch
            {
                isServerActive = false;
                server.Stop();
                SetText(listBoxServer, "Connection closed/aborted.");
            }
        }
    }
}
