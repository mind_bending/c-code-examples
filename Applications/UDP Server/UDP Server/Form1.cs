﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace UDS_Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int port = (int)numericUpDown1.Value;
            IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                UdpClient server = new UdpClient(port);
                Byte[] data = server.Receive(ref remoteIP);
                string message = Encoding.ASCII.GetString(data);
                listBox1.Items.Add(message);
                server.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
