﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace Remote_Screenshot_Client
{
    public partial class Form1 : Form
    {
        private int commandsServerPort = 1978;
        private IPAddress dataServerIP = IPAddress.Parse("127.0.0.1");
        private int dataServerPort = 25000;
        private string localIPAddress;
        private Bitmap image;

        public Form1()
        {
            InitializeComponent();
            IPHostEntry adresyIP = Dns.GetHostEntry(Dns.GetHostName());
            localIPAddress = "127.0.0.1";
            backgroundWorker1.RunWorkerAsync();
        }

        private Bitmap makeScreenshot()
        {
            Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
            Graphics screenshot = Graphics.FromImage(bitmap);
            screenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size,
            CopyPixelOperation.SourceCopy);
            return bitmap;
        }

        delegate void SetTextCallBack(string text);

        private void SetText(string text)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { text });
            }
            else
            {
                this.listBox1.Items.Add(text);
            }
        }

        private void SendUDPMessage(string message)
        {
            UdpClient client = new UdpClient(dataServerIP.ToString(), 43210);
            byte[] buffer = Encoding.ASCII.GetBytes(message);
            client.Send(buffer, buffer.Length);
            client.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            TcpListener server = new TcpListener(IPAddress.Parse(localIPAddress), commandsServerPort);
            server.Start();
            this.SetText("Waiting for commands ...");
            while (true)
            {
                TcpClient commandsClient = server.AcceptTcpClient();
                this.SetText("Command received.");
                NetworkStream ns = commandsClient.GetStream();
                Byte[] buffer = new Byte[5];
                int odczyt = ns.Read(buffer, 0, buffer.Length);
                String s = Encoding.ASCII.GetString(buffer);
                string message = Encoding.ASCII.GetString(buffer);
                if (message == "##S##")
                {
                    this.SetText("Making screenshot in progress ...");
                    image = makeScreenshot();
                    MemoryStream ms = new MemoryStream();
                    image.Save(ms, ImageFormat.Jpeg);
                    byte[] imageByte = ms.GetBuffer();
                    ms.Close();
                    try
                    {
                        TcpClient client2 = new
                        TcpClient(dataServerIP.ToString(), dataServerPort);
                        NetworkStream ns2 = client2.GetStream();
                        this.SetText("Sending screenshot ...");
                        using (BinaryWriter bw = new BinaryWriter(ns2))
                        {
                            bw.Write((int)imageByte.Length);
                            bw.Write(imageByte);
                        }
                        this.SetText("Screenshot was sent.");
                    }
                    catch //(Exception ex)
                    {
                        this.SetText("Cannot connect with the server");
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SendUDPMessage(localIPAddress + ":HI");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SendUDPMessage(localIPAddress + ":BYE");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendUDPMessage(localIPAddress + ":HI");
        }

    }
}
