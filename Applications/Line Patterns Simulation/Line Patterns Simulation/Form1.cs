﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Line_Patterns_Simulation
{
    public partial class Form1 : Form
    {
        Pen pen = new Pen(Color.Black);
        Graphics g = null;

        static int start_x, start_y;
        static int end_x, end_y;

        static int angle = 0;
        static int length = 0;
        static int increment = 0;

        public Form1()
        {
            InitializeComponent();
            
            g = canvas.CreateGraphics();
            pen.Width = 1;

            start_x = canvas.Width / 2;
            start_y = canvas.Height / 2;
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            length = Int32.Parse(txtLength.Text);

            for(int i = 0; i <Int32.Parse(txtLinesNumber.Text); i++)
            {
                drawLine();
            }
        }

        private void drawLine(){

            angle = angle + Int32.Parse(txtAngle.Text);
            length = length + Int32.Parse(txtIncrement.Text);

            // .017453292519 - radians to degrees conversion
            end_x = (int)(start_x + Math.Cos(angle * .017453292519) * length);
            end_y = (int)(start_y + Math.Sin(angle * .017453292519) * length);

            Point[] points = 
            {
              new Point(start_x, start_y),
              new Point(end_x, end_y)   
            };

            start_x = end_x;
            start_y = end_y;

            g.DrawLines(pen, points);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            length = Int32.Parse(txtLength.Text);
            increment = Int32.Parse(txtIncrement.Text);
            angle = Int32.Parse(txtAngle.Text);

            start_x = canvas.Width / 2;
            start_y = canvas.Height / 2;

            canvas.Refresh();
        }
    }
}
