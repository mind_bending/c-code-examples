﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace NetDetector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void SendAsynchronousPing(string address, int timeout, byte[] buffer, PingOptions options)
        {
            Ping ping = new Ping();
            ping.PingCompleted += new PingCompletedEventHandler(EndPing);
            try
            {
                ping.SendAsync(address, timeout, buffer, options, null);
            }
            catch (Exception ex)
            {
                listBox1.Items.Add("Error: " + address + " " + ex.Message);
            }
        }
        //End of SendAsynchronousPing

        public void EndPing(object sender, PingCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                listBox1.Items.Add("Error: Operation Aborted or Incorrect Address ");
                ((IDisposable)(Ping)sender).Dispose();
                return;
            }
            PingReply response = e.Reply;
            if (response.Status == IPStatus.Success)
            {
                listBox1.Items.Add("Response " + response.Address.ToString() + "bytes=" + response.Buffer.Length + " time=" +
                response.RoundtripTime + "ms TTL=" + response.Options.Ttl);
            }
            else
            {
                listBox1.Items.Add("Error: No response from " + e.Reply.Address + ":" + response.Status);
                ((IDisposable)(Ping)sender).Dispose();
            }
        }
        //End of EndPing

        private void button1_Click(object sender, EventArgs e)
       {
            listBox1.Items.Clear();
            IPAddress startIP = null;
            IPAddress endIP = null;
            try
            {
                startIP = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("The initial(start) IP adress is incorrect", "Error");
                textBox1.Text = string.Empty;
                return;
            }
            try
            {
                endIP = IPAddress.Parse(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("The final(end) IP adress is incorrect", "Error");
                textBox2.Text = string.Empty;
                return;
            }
            byte[] start = startIP.GetAddressBytes();
            byte[] end = endIP.GetAddressBytes();
            PingOptions options = new PingOptions();
            options.Ttl = 128;
            options.DontFragment = true;
            string dane = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] bufor = Encoding.ASCII.GetBytes(dane);
            int timeout = 120;
            for (byte oktet1 = start[0]; oktet1 <= end[0]; oktet1++)
                for (byte oktet2 = start[1]; oktet2 <= end[1]; oktet2++)
                    for (byte oktet3 = start[2]; oktet3 <= end[2]; oktet3++)
                        for (byte oktet4 = start[3]; oktet4 <= end[3]; oktet4++)
                        {
                            IPAddress adres = new IPAddress(new byte[] { oktet1,oktet2, oktet3, oktet4 });
                            SendAsynchronousPing(adres.ToString(), timeout, bufor,options);
                        }
           }
        
    }
}
