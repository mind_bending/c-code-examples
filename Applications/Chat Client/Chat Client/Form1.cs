﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Chat_Client
{
    public partial class Form1 : Form
    {
        private TcpClient client;
        private string addressIPServer = "127.0.0.1";
        private BinaryWriter write;
        private bool isActive = false;

        public Form1()
        {
            InitializeComponent();
            webBrowserChat.Document.Write("<html><head><style>body,table{ font-size: 10pt; font-family: Verdana; margin: 3px 3px 3px 3px;font-color: black;}</style></head><body width=\"" + "\">");
        }


        // Delegates + methods:
        delegate void SetTextCallBack(ListBox list, string text);
        private void SetText(ListBox list, string text)
        {
            if (list.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { list, text });
            }
            else
            {
                list.Items.Add(text);
            }
        }
        delegate void SetTextHTMLCallBack(string text);
        private void SetTextHTML(string text)
        {
            if (webBrowserChat.InvokeRequired)
            {
                SetTextHTMLCallBack f = new SetTextHTMLCallBack(SetTextHTML);
                this.Invoke(f, new object[] { text });
            }
            else
            {
                this.webBrowserChat.Document.Write(text);
            }
        }
        delegate void SetScrollCallBack();
        private void SetScroll()
        {
            if (webBrowserChat.InvokeRequired)
            {
                SetScrollCallBack f = new SetScrollCallBack(SetScroll);
                this.Invoke(f);
            }
            else
            {
                this.webBrowserChat.Document.Window.ScrollTo(1, int.MaxValue);
            }
        }

        private void AddText(string who, string message)
        {
            SetTextHTML("<table><tr><td width=\"10%\"><b>[" + who + "]:</b></td>");
            SetTextHTML("<td colspan=2>" + message + "</td></tr></table>");
            SetScroll();
        }

        //


        private void buttonConnect_Click(object sender, EventArgs e)
        {
            //
            try
            {
                if (textBoxNick.Text != String.Empty)
                {
                    client = new TcpClient(addressIPServer, 25000);
                    textBoxNick.ReadOnly = true;
                    NetworkStream ns = client.GetStream();
                    write = new BinaryWriter(ns);
                    write.Write(textBoxNick.Text + ":HI:" + "empty");
                    BinaryReader read = new BinaryReader(ns);
                    string answer = read.ReadString();
                    if (answer == "HI")
                    {
                        backgroundWorkerMainThread.RunWorkerAsync();
                        isActive = true;
                        buttonConnect.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Server is refusing connection.");
                        buttonConnect.Enabled = true; client.Close();
                    }
                }
                else
                    MessageBox.Show("Enter your nick.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection cannot be established " + ex.Message);
            }
            //
        }

        private void backgroundWorkerMainThread_DoWork(object sender, DoWorkEventArgs e)
        {
            UdpClient client = new UdpClient(43210);
            IPEndPoint addressIP = new IPEndPoint(IPAddress.Parse(addressIPServer), 43210);
            string message = "";
            while (!backgroundWorkerMainThread.CancellationPending)
            {
                Byte[] buffer = client.Receive(ref addressIP);
                string data = Encoding.UTF8.GetString(buffer);
                string[] cmd = data.Split(new char[] { ':' });
                if (cmd[1] == "BYE")
                {
                    AddText("system", "client disconnected");
                    client.Close();
                    return;
                }
                if (cmd.Length > 2)
                {
                    message = cmd[2];
                    for (int i = 3; i < cmd.Length; i++)
                        message += ":" + cmd[i];
                }
                AddText(cmd[0], message);
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (isActive && textBoxMessage.Text != String.Empty)
                write.Write(textBoxNick.Text + ":SAY:" + textBoxMessage.Text);
            textBoxMessage.Text = String.Empty;
        }

        private void textBoxMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.buttonSend_Click(sender, null);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (write != null)
            {
                try
                {
                    write.Write(textBoxNick.Text + ":BYE:" + "empty");
                    write.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
            if (backgroundWorkerMainThread.IsBusy)
                backgroundWorkerMainThread.CancelAsync();
            if (client != null)
                client.Close();
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
