﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Messsanger_Client
{
    public partial class Form1 : Form
    {
        delegate void SetTextCallBack(string tekst);
        delegate void SetScrollCallBack();

        private TcpClient client;
        private BinaryWriter writing;
        private BinaryReader reading;
        private string serwerIP = "127.0.0.1";
        private int cursorPosition;
        private bool isConnectionAlive;

        public Form1()
        {
            InitializeComponent();
            this.webBrowser1.Navigate("about:blank");
            this.webBrowser1.Document.Write("<html><head><style>body,table { font-´size: 10pt; font-family: Verdana; margin: 3px 3px 3px 3px; font-color:black;}</style></head><body width=\"" + (webBrowser1.ClientSize.Width - 20).ToString() + "\">");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void SetText(string tekst)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.listBox1.Items.Add(tekst);
            }
        }
        private void SetTextHTML(string tekst)
        {
            if (webBrowser1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetTextHTML);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.webBrowser1.Document.Write(tekst);
            }
        }
        private void SetScroll()
        {
            if (webBrowser1.InvokeRequired)
            {
                SetScrollCallBack s = new SetScrollCallBack(SetScroll);
                this.Invoke(s);
            }
            else
            {
                this.webBrowser1.Document.Window.ScrollTo(1, Int32.MaxValue);
            }
        }

        private void WriteText(string who, string message)
        {
            SetTextHTML("<table><tr><td width=\"10%\"><b>" + who + "</b></td><td width=\"90%\">(" + DateTime.Now.ToShortTimeString() + "):</td></tr>");
            SetTextHTML("<tr><td colspan=2>" + message + "</td></tr></table>");
            SetTextHTML("<hr>");
            SetScroll();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                client = new TcpClient(serwerIP, (int)numericUpDown1.Value);
                NetworkStream ns = client.GetStream();
                reading = new BinaryReader(ns);
                writing = new BinaryWriter(ns);
                writing.Write("###HI###");
                this.SetText("Authorization ...");
                isConnectionAlive = true;
                backgroundWorker2.RunWorkerAsync();
            }
            catch
            {
                this.SetText("Connection cannot be established.");
                isConnectionAlive = false;
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            SetText("Authorization finished.");
            string message;
            try
            {
                while ((message = reading.ReadString()) != "###BYE###")
                {
                    WriteText("someone", message);
                }
                SetText("Connection closed.");
                isConnectionAlive = false;
                client.Close();
            }
            catch
            {
                SetText("Connection with server was closed.");
                isConnectionAlive = false;
                client.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isConnectionAlive == false)
            {
                backgroundWorker1.RunWorkerAsync();
                webBrowser1.Navigate("about:blank");
            }
            else
            {
                isConnectionAlive = false;
                if (client != null)
                {
                    writing.Write("###BYE###");
                    client.Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WriteText("Me", textBox1.Text);
            try
            {
                if (isConnectionAlive)
                    writing.Write(textBox1.Text);
            }
            catch
            {
                textBox1.Text = "No client connected";
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.webBrowser1.Navigate("about:blank");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    try
                    {
                        sw.Write(webBrowser1.DocumentText);
                    }
                    catch
                    {
                        MessageBox.Show("File cannot be saved: " + saveFileDialog1.FileName);
                    }
            }
        }
    }
}
