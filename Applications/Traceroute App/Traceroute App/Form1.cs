﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace Traceroute_App
{
    public partial class Form1 : Form
    {
        private Ping ping = new Ping();
        private byte[] buffer;
        private int i = 1;

        public Form1()
        {
            InitializeComponent();
            ping.PingCompleted += new PingCompletedEventHandler(ping_PingCompleted);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            i = 1;
            PingOptions pingOptions = new PingOptions(i, true);
            string s = null;
            if (textBox1.Text != "" && textBox1.Text.Trim() != "")
            {
                int bufferSize = 32;
                try
                {
                    bufferSize = System.Convert.ToInt16(comboBox1.Text);
                }
                catch
                {
                    bufferSize = 32;
                    comboBox1.Text = "32";
                }
                for (int j = 0; j < bufferSize; j++)
                {
                    s += "a";
                }
                buffer = Encoding.ASCII.GetBytes(s);
                ping.SendAsync(textBox1.Text, (int)numericUpDown2.Value, buffer, pingOptions, null);
                listBox1.Items.Add("Starting route analysis to " +
                textBox1.Text);
                button1.Enabled = false;
            }
        }
        //End of Button1_Click

        void ping_PingCompleted(object sender, PingCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                listBox1.Items.Add(e.Error.Message);
                return;
            }
            if (e.Cancelled)
            {
                listBox1.Items.Add("Operation aborted by user.");
            }
            else
            {
                if (e.Reply.Status == IPStatus.TtlExpired)
                    listBox1.Items.Add("Step " + i.ToString() + " host: " +
                    e.Reply.Address.ToString());
                if (e.Reply.Status == IPStatus.TimedOut)
                    listBox1.Items.Add("Step " + i.ToString() + " host: * request timeout.");
                if (e.Reply.Status == IPStatus.Success)
                {
                    listBox1.Items.Add("Step " + i.ToString() + " host: " + e.Reply.Address.ToString());
                    button1.Enabled = true;
                    return;
                    
                }
                if (i++ < (int)numericUpDown1.Value)
                {
                    PingOptions opcjePing = new PingOptions(i, true);
                    ping.SendAsync(textBox1.Text, (int)numericUpDown2.Value, buffer,
                    opcjePing, null);
                }
                else
                {
                    listBox1.Items.Add("Max number of step exceeded (parameter TTL = " + numericUpDown1.Value.ToString());
                    button1.Enabled = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ping.SendAsyncCancel();
            button1.Enabled = true;
        }

    }
}
