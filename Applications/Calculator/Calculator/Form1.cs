﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        Double value = 0;
        String operation = "";
        bool operationPressed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonClick(object sender, EventArgs e)
        {
            if ((txtResult.Text == "0")||(operationPressed))
                txtResult.Clear();

            operationPressed = false;
            Button b = (Button)sender;
            if (b.Text == ",")
            {
                if (!txtResult.Text.Contains(","))
                    txtResult.Text = txtResult.Text + b.Text;
            }
            else
            {
                txtResult.Text = txtResult.Text + b.Text;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            txtResult.Text = "0";
            lblEquasion.Text = "";
        }

        private void operationClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (value != 0)
            {
                equal.PerformClick();
                operationPressed = true;
                operation = b.Text;
                lblEquasion.Text = value + " " + operation;
            }
            else
            {
                operation = b.Text;
                value = Double.Parse(txtResult.Text);
                operationPressed = true;
                lblEquasion.Text = value + " " + operation; 
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            lblEquasion.Text = "";
            
            switch (operation)
            {
                case "+":
                    txtResult.Text = (value + Double.Parse(txtResult.Text)).ToString();
                    break;
                case "-":
                    txtResult.Text = (value - Double.Parse(txtResult.Text)).ToString();
                    break;
                case "*":
                    txtResult.Text = (value * Double.Parse(txtResult.Text)).ToString();
                    break;
                case "/":
                    txtResult.Text = (value / Double.Parse(txtResult.Text)).ToString();
                    break;
                default:
                    break;
            }

            value = Double.Parse(txtResult.Text);
            operation = "";
        }

        private void button17_Click(object sender, EventArgs e)
        {
            txtResult.Text = "0";
            lblEquasion.Text = "";
            value = 0;
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "0":
                    zero.PerformClick();
                    break;
                case "1":
                    one.PerformClick();
                    break;
                case "2":
                    two.PerformClick();
                    break;
                case "3":
                    three.PerformClick();
                    break;
                case "4":
                    four.PerformClick();
                    break;
                case "5":
                    five.PerformClick();
                    break;
                case "6":
                    six.PerformClick();
                    break;
                case "7":
                    seven.PerformClick();
                    break;
                case "8":
                    eight.PerformClick();
                    break;
                case "9":
                    nine.PerformClick();
                    break;
                case "+":
                    add.PerformClick();
                    break;
                case "-":
                    sub.PerformClick();
                    break;
                case "*":
                    times.PerformClick();
                    break;
                case "/":
                    div.PerformClick();
                    break;
                case "=":
                    equal.PerformClick();
                    break;
                default:
                    break;
            }
        }
    }
}
