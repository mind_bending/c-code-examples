﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AccessDatabaseApp
{
    public partial class Employees : Form
    {
        private OleDbConnection connection = new OleDbConnection();

        public Employees()
        {
            InitializeComponent();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Artur\Documents\EmployeeData.accdb;Persist Security Info=False;";
            initDataGrid();
        }



        private void btnSave_Click_1(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "INSERT INTO EmployeeInfo (FirstName, LastName, Pay) VALUES('" + txtFirstName.Text + "', '" + txtLastName.Text + "', '" + double.Parse(txtPay.Text) + "')";

                command.ExecuteNonQuery();
                MessageBox.Show("Data was successfully saved into database.");
                connection.Close();
                initDataGrid();

            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;

                String query = "UPDATE EmployeeInfo SET FirstName='" + txtFirstName.Text + "', LastName='" + txtLastName.Text + "' ,Pay='" + double.Parse(txtPay.Text) + "' WHERE EID=" + txtEID.Text+";";
                command.CommandText = query; 
                command.ExecuteNonQuery();
                MessageBox.Show("Data edited successfully.");
                connection.Close();
                initDataGrid();

            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;

                String query = "DELETE FROM EmployeeInfo WHERE EID=" + txtEID.Text +";";
                command.CommandText = query;
                command.ExecuteNonQuery();
                MessageBox.Show("Data was deleted.");
                connection.Close();
                initDataGrid();

            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }

        private void Employees_Load(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;

                String query = "SELECT * FROM EmployeeInfo";
                command.CommandText = query;

                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    comboBox1.Items.Add(reader["EID"].ToString() + " " + reader["FirstName"].ToString() + " " + reader["LastName"].ToString());
                }
           
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                if (comboBox1.SelectedItem != null)
                {
                    String query = "SELECT * FROM EmployeeInfo WHERE EID =" + comboBox1.Text.Split()[0] + ";";
                    command.CommandText = query;

                    OleDbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        txtEID.Text = reader["EID"].ToString();
                        txtFirstName.Text = reader["FirstName"].ToString();
                        txtLastName.Text = reader["LastName"].ToString();
                        txtPay.Text = reader["Pay"].ToString();
                    }
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }

        private void initDataGrid()
        {

            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;

                String query = "SELECT EID, FirstName, LastName, Pay FROM EmployeeInfo";
                command.CommandText = query;

                OleDbDataAdapter da = new OleDbDataAdapter(command);

                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Width = 50;
                dataGridView1.Columns[1].Width = 80;
                dataGridView1.Columns[2].Width = 80;
                dataGridView1.Columns[3].Width = 55;

                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }

        private void btnShowChart_Click(object sender, EventArgs e)
        {
            Graph graphForm = new Graph();
            graphForm.Show();
   
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;

                    String query = "SELECT * FROM EmployeeInfo";
                    command.CommandText = query;

                    OleDbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        graphForm.chart1.Series["PAY"].Points.AddXY(reader["FirstName"].ToString(), reader["Pay"].ToString());
                        txtFirstName.Text = reader["FirstName"].ToString();
                        txtLastName.Text = reader["LastName"].ToString();
                        txtPay.Text = reader["Pay"].ToString();
                    }
                graphForm.Show();
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Error: " + ex);
            }
        }
  
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                new Form1().ShowDialog();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0) {

                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                txtEID.Text = row.Cells[0].Value.ToString();
                txtFirstName.Text = row.Cells[1].Value.ToString();
                txtLastName.Text = row.Cells[2].Value.ToString();
                txtPay.Text = row.Cells[3].Value.ToString();

            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            foreach (DataGridViewCell selectedCell in dataGridView1.SelectedCells)
            {
                cell = selectedCell;
                break;
            }
            if (cell != null)
            {
                DataGridViewRow row = cell.OwningRow;
                txtEID.Text = row.Cells[0].Value.ToString();
                txtFirstName.Text = row.Cells[1].Value.ToString();
                txtLastName.Text = row.Cells[2].Value.ToString();
                txtPay.Text = row.Cells[3].Value.ToString();
            }
        }
    }
}
