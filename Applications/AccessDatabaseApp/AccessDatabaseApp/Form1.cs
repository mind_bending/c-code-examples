﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AccessDatabaseApp
{
    public partial class Form1 : Form
    {
        private OleDbConnection connection = new OleDbConnection();

        public Form1()
        {
            InitializeComponent();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Artur\Documents\EmployeeData.accdb;Persist Security Info=False;";
                
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

               
                connection.Open();
                connection.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM EmployeeInfo WHERE UserName = '" + txtUserName.Text + "' AND Password = '" + txtPassword.Text + "'";

           
            OleDbDataReader reader = command.ExecuteReader();
            int count = 0;

            while (reader.Read()) 
            {
                count++;
            }

            if (count == 1)
            {
                MessageBox.Show("Username and Password are correct.");
                connection.Close();
                connection.Dispose();
                this.Hide();
                Employees employeesForm = new Employees();
                employeesForm.ShowDialog();

            }
            else if (count > 1)
            {
                MessageBox.Show("Duplicated Username and Password.");
            }
            else {
                MessageBox.Show("Username and Password are incorrect!");
            }

            connection.Close();
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
