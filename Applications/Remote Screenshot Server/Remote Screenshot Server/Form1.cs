﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.IO;
using System.Management.Instrumentation;

namespace Screenshot_Server
{
    public partial class Form1 : Form
    {
        private Image image = null;
        static int counter = 0;

        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.RunWorkerAsync();
            backgroundWorker2.RunWorkerAsync();
        }

        delegate void SetTextCallBack(string tekst);
        delegate void RemoveTextCallBack(int pozycja);


        private void SetText(string tekst)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.listBox1.Items.Add(tekst);
            }
        }

        private void RemoveText(int pozycja)
        {
            if (listBox1.InvokeRequired)
            {
                RemoveTextCallBack f = new RemoveTextCallBack(RemoveText);
                this.Invoke(f, new object[] { pozycja });
            }
            else
            {
                listBox1.Items.RemoveAt(pozycja);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
            UdpClient client = new UdpClient(43210);
            while (true)
            {
                Byte[] buffer = client.Receive(ref remoteIP);
                string data = Encoding.ASCII.GetString(buffer);
                string[] cmd = data.Split(new char[] { ':' });
                if (cmd[1] == "HI")
                {
                    foreach (string entry in listBox1.Items)
                        if (entry == cmd[0])
                        {
                            MessageBox.Show("Connection attempt with " +
                            cmd[0] + " refused, because entry already exists on the list.");
                            return;
                        }
                    this.SetText(cmd[0]);
                }
                if (cmd[1] == "BYE")
                {
                    for (int i = 0; i < listBox1.Items.Count; i++)
                        if (listBox1.Items[i].ToString() == cmd[0])
                            this.RemoveText(i);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                return;
            try
            {
                TcpClient client = new TcpClient(listBox1.Items[listBox1.SelectedIndex].ToString(), 1978);
                NetworkStream ns = client.GetStream();
                byte[] buffer = new byte[5];
                buffer = Encoding.ASCII.GetBytes("##S##");
                ns.Write(buffer, 0, buffer.Length);
                if (backgroundWorker1.IsBusy == false)
                    backgroundWorker1.RunWorkerAsync();
                else
                    MessageBox.Show("Screenshot cannot be created at this moment.");
            }
            catch
            {
                MessageBox.Show("Error: Connection cannot be established.");
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {    
            TcpListener server2 = new TcpListener(IPAddress.Parse(textBox1.Text), (int)numericUpDown1.Value);
            server2.Start();
            TcpClient client2 = server2.AcceptTcpClient();
            NetworkStream ns = client2.GetStream();
            byte[] obrazByte;
            using (BinaryReader imageRead = new BinaryReader(ns))
            {
                int imageSize = imageRead.ReadInt32();
                obrazByte = imageRead.ReadBytes(imageSize);
            }
            using (MemoryStream ms = new MemoryStream(obrazByte))
            {
                image = Image.FromStream(ms);
                pictureBox1.Image = image;
            }
            server2.Stop();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void saveScreenshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "PNG Files(*.png)|*.png|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "Screenshot" + counter;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (image != null)
                {
                    Bitmap bmp = new Bitmap(image);
                    bmp.Save(saveFileDialog1.FileName);
                    counter++;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
