﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net;
namespace netinfo
{
    class Program
    {
        static void Main(string[] args)
        {
            IPGlobalProperties wlasnosciIP =
            IPGlobalProperties.GetIPGlobalProperties();
            Console.WriteLine("Host Name: " + wlasnosciIP.HostName);
            Console.WriteLine("Domain Name: " + wlasnosciIP.DomainName);
            Console.WriteLine();
            int licznik = 0;
            foreach (NetworkInterface kartySieciowe in NetworkInterface.GetAllNetworkInterfaces())
            {
                Console.WriteLine("Card #" + ++licznik + ": " + kartySieciowe.Id);
                Console.WriteLine(" Address MAC: " + kartySieciowe.GetPhysicalAddress().ToString());
                Console.WriteLine(" Name: " + kartySieciowe.Name);
                Console.WriteLine(" Description: " + kartySieciowe.Description);
                Console.WriteLine(" Status: " + kartySieciowe.OperationalStatus);
                Console.WriteLine(" Speed: " + (kartySieciowe.Speed) / (double)1000000 + " Mb/s");
                Console.WriteLine(" Adressess of Network Gateways:");
                foreach (GatewayIPAddressInformation adresBramy in kartySieciowe.GetIPProperties().GatewayAddresses)
                    Console.WriteLine(" " + adresBramy.Address.ToString());
                Console.WriteLine(" DNS Servers:");
                foreach (IPAddress adresIP in kartySieciowe.GetIPProperties().DnsAddresses)
                    Console.WriteLine(" " + adresIP.ToString());
                Console.WriteLine(" DHCP Servers:");
                foreach (IPAddress adresIP in kartySieciowe.GetIPProperties().DhcpServerAddresses)
                    Console.WriteLine(" " + adresIP.ToString());
                Console.WriteLine(" WINS Servers:");
                foreach (IPAddress adresIP in kartySieciowe.GetIPProperties().WinsServersAddresses)
                    Console.WriteLine(" " + adresIP.ToString());
                Console.WriteLine();
            }
            Console.WriteLine(" Present TCP/IP connection - Client Type:");
            foreach (TcpConnectionInformation polaczenieTCP in IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections())
            {
                Console.WriteLine(" Remote address: " + polaczenieTCP.RemoteEndPoint.Address.ToString() + ":" + polaczenieTCP.RemoteEndPoint.Port);
                Console.WriteLine(" Status: " + polaczenieTCP.State.ToString());
            }
            Console.WriteLine(" Present TCP/IP connection - Server Type:");
            foreach (IPEndPoint polaoczenieTCP in IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners())
                Console.WriteLine(" Remote address: " + polaoczenieTCP.Address.ToString() + ":" + polaoczenieTCP.Port);
            Console.WriteLine(" Going UDP connections:");
            foreach (IPEndPoint polaczenieUDP in IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners())
                Console.WriteLine(" Remote address:" + polaczenieUDP.Address.ToString() + ":" + polaczenieUDP.Port);

            Console.ReadKey();
        }
    }
}