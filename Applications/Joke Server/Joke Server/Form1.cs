﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace My_Simple_Messaging_Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != string.Empty && textBox2.Text.Trim().Length > 0)
            {
                listBox1.Items.Add(textBox2.Text);
                textBox2.Text = String.Empty;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count > 0)
            {
                for (int i = listBox1.Items.Count - 1; i >= 0; i--)
                    if (listBox1.GetSelected(i))
                        listBox1.Items.RemoveAt(i);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index > -1)
            {
                string position = listBox1.Items[index].ToString();
                listBox1.Items.RemoveAt(index);
                if (position.StartsWith("(Blocked) "))
                    listBox1.Items.Insert(index, position.Remove(0, "(Blocked) ".Length));
                else
                    listBox1.Items.Insert(index, "(Blocked) " + position);
            }
        }

        private void openContactsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count > 0)
                if (MessageBox.Show("Reading a file will cause that current list will be deleted", "Warning ...",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) ==
                DialogResult.Cancel)
                    return;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new
                StreamReader(openFileDialog1.FileName))
                {
                    string line;
                    listBox1.Items.Clear();
                    while ((line = sr.ReadLine()) != null)
                        listBox1.Items.Add(line);
                }
            }
        }

        private void saveContactsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new
                StreamWriter(saveFileDialog1.FileName))
                {
                    foreach (string wpis in listBox1.Items)
                        sw.WriteLine(wpis);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "Closing server...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
            else
                e.Cancel = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Byte[] buffer = Encoding.ASCII.GetBytes(textBox1.Text);
            foreach (string host in listBox1.Items)
            {
                try
                {
                    if (host.StartsWith("(Blocked) ") == false)
                        using (UdpClient client = new UdpClient(host, (int)numericUpDown1.Value))
                        {
                            client.Send(buffer, buffer.Length);
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Connection cannot be established." + ex.Message, "Error");
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
             MessageBox.Show("Joke Server by Artur Wieczorek.\nServer is running on port 25000 by default.\nThis is a part of Joke Client-Server Project.","Information");
        }

    }
}
