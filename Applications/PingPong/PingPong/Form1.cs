﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        private int speed_left = 4;
        private int speed_top = 4;
        private int score = 0;

        public Form1()
        {
            InitializeComponent();

            lblGameOver.Left = (playground.Width / 2) - (lblGameOver.Width / 2);
            lblGameOver.Top = (playground.Height / 2) - (lblGameOver.Height / 2);

            timer1.Enabled = true;
            //Cursor.Hide();

            //this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true; //Bring the Form to the Front
            //this.Bounds = Screen.PrimaryScreen.Bounds; //Full Screen Mode
            racket.Top = playground.Bottom - (playground.Bottom / 20);
        }
       

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) {
                this.Close();
            }

            if (e.KeyCode == Keys.F1)
            {
                score = 0;
                lblScore.Text = score.ToString();

                lblGameOver.Visible = false;

                ball.Location = new Point(100, 100);
                speed_left = 4;
                speed_top = 4;
                timer1.Enabled = true;
            }
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            racket.Left = Cursor.Position.X - (racket.Width);

            ball.Left += speed_left;
            ball.Top += speed_top;

            //Racket collision
            if (ball.Bottom >= racket.Top && ball.Bottom <= racket.Bottom && ball.Left >= racket.Left && ball.Right <= racket.Right)
            {
                speed_top += 2;
                speed_left += 2;
                //change Y direction after collision
                speed_top = -speed_top;
                score += 1;
                lblScore.Text = score.ToString();

                Random r = new Random();
                playground.BackColor = Color.FromArgb(r.Next(150, 255), r.Next(150, 255), r.Next(150, 255));
            }

            if (ball.Left <= playground.Left) {
                speed_left = -speed_left;
            }

            if (ball.Right >= playground.Right)
            {
                speed_left = -speed_left;
            }

            if (ball.Top <= playground.Top)
            {
                speed_top = - speed_top;
            }

            if (ball.Bottom >= playground.Bottom)
            {
                timer1.Enabled = false;
                lblGameOver.Visible = true;
            }


        }

    }
}
