﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;


namespace Send_Mail_With_Attachment
{
    public partial class Form1 : Form
    {
        bool debug = false;
        private MailAddress From;
        private MailAddress To;
        private MailAddress Cc;
        private MailAddress Bc;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            SmtpClient client = null;
            try
            {
                From = new MailAddress(textBoxOd.Text);
            }
            catch
            {
                MessageBox.Show("Wrong sender's e-mail address");
                //textBoxOd.Text = String.Empty;
                textBoxOd.Focus();
                textBoxOd.SelectAll();
                return;
            }
            try
            {
                To = new MailAddress(textBoxDo.Text);
            }
            catch
            {
                MessageBox.Show("Wrong receiver's e-mail address");
                //textBoxDo.Text = String.Empty;
                textBoxOd.Focus();
                textBoxDo.SelectAll();
                return;
            }
            if (textBoxCC.Text != string.Empty)
                if (textBoxCC.Text.Trim().Length > 0)
                    try
                    {
                        Cc = new MailAddress(textBoxCC.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Wrong CC receivers e-mail addresse(s)");
                        //textBoxCC.Text = String.Empty;
                        textBoxCC.Focus();
                        textBoxCC.SelectAll();
                        return;
                    }

            if (textBoxBc.Text != string.Empty)
                if (textBoxBc.Text.Trim().Length > 0)
                    try
                    {
                        Bc = new MailAddress(textBoxBc.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Nieprawidłowy adres e-mail odbiorców ukrytych"); textBoxBc.Focus();
                        textBoxBc.SelectAll();
                        return;
                    }

            MailMessage message = new MailMessage(From, To);
            message.Subject = textBoxTemat.Text;
            message.Body = textBoxList.Text;
            if (Cc != null)
                message.CC.Add(Cc);
            if (Bc != null)
                message.Bcc.Add(Bc);
            if (listBox2.Items.Count > 0)
                foreach (string plik in listBox2.Items)
                {
                    Attachment zalacznik = new Attachment(plik);
                    message.Attachments.Add(zalacznik);
                }
            message.IsBodyHtml = checkBox1.Checked;
            try
            {
                client = new SmtpClient();
                client.Port = Int16.Parse(textBoxPort.Text);
                client.Host = textBoxAdres.Text;
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                if (textBoxUser.Text != String.Empty && maskedTextBoxPass.Text != String.Empty)
                    client.Credentials = new NetworkCredential(textBoxUser.Text, maskedTextBoxPass.Text);

                if (debug)
                {
                    MessageBox.Show("Host: " + client.Host.ToString() + "|Port: " + client.Port.ToString()
                        + "|User: " + textBoxUser.Text + "|From: " + message.From.ToString()
                        + "|To: " + message.To.ToString() + "|Subject: " + message.Subject.ToString() 
                        + "|Body: " + message.Subject.ToString(), "INFO");
                }
                
                client.Send(message);
                txtServerResponse.AppendText("Message was sent!");
            }
            catch (Exception ex)
            {
                 txtServerResponse.AppendText("No connection with server :" + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                listBox2.Items.Add(openFileDialog1.FileName);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBoxOd.Clear();
            textBoxDo.Clear();
            textBoxCC.Clear();
            textBoxBc.Clear();
            textBoxTemat.Clear();
            textBoxList.Clear();
            txtServerResponse.Clear();
        }
    }
}
