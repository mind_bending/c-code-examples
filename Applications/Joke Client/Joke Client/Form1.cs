﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Joke_Client
{
    public partial class Form1 : Form
    {
        bool closing = false;
        delegate void SetTextCallBack(string tekst);

        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.RunWorkerAsync();
        }

        private void showForm()
        {
            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
        }
        private void HideForm()
        {
            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
        }
        private void CloseForm()
        {
            closing = true;
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closing == false)
            {
                e.Cancel = true;
                HideForm();
                if (backgroundWorker1.IsBusy == false)
                    backgroundWorker1.RunWorkerAsync();
            }
            else
                notifyIcon1.Visible = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showForm();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy == false)
                backgroundWorker1.RunWorkerAsync();
            HideForm();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            showForm();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            notifyIcon1.BalloonTipText = "New Joke ...";
            notifyIcon1.ShowBalloonTip(15);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (textBox2.Text == string.Empty) {
                textBox2.Text = "127.0.0.1";
            }
            using (UdpClient client = new UdpClient(25000))
            {
                IPEndPoint IPSerwera = new IPEndPoint(IPAddress.Parse(textBox2.Text), 25000);
                Byte[] read = client.Receive(ref IPSerwera);
                string text = Encoding.ASCII.GetString(read);
                this.SetText(text);
            }
        }

        private void SetText(string text)
        {
            if (textBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { text });
            }
            else
            {
                this.textBox1.Text = text;
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Joke Client by Artur Wieczorek.\nServer is running on port 25000 by default.\nThis is a part of Joke Client-Server Project.","Information");
        }
    }
}
