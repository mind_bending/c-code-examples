﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Messanger_Server
{
    public partial class Form1 : Form
    {
        private int CursorPosition;
        private TcpListener server = null;
        private TcpClient client = null;
        private string adresIP = "127.0.0.1";
        private BinaryReader reading = null;
        private BinaryWriter writing = null;
        private bool isConnectionAlive = false;
        delegate void SetTextCallBack(string tekst);
        delegate void SetScrollCallBack();

        public Form1()
        {
            InitializeComponent();
            webBrowser1.Navigate("about:blank");
            webBrowser1.Document.Write("<html><head><style>body,table { font-size:10pt; font-family: Verdana; margin: 3px 3px 3px 3px; font-color:black;}</style></head><body width=\"" + (webBrowser1.ClientSize.Width - 20).ToString() + "\">");
            IPHostEntry adresyIP = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress pozycja in adresyIP.AddressList)
                comboBox1.Items.Add(pozycja.ToString());
            //
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void EnterTag(string tag)
        {
            string kod = textBox1.Text;
            textBox1.Text = kod.Insert(CursorPosition, tag);
            textBox1.Focus();
            if (tag == "<br>" || tag == "<hr>")
            {
                textBox1.Select(CursorPosition + tag.Length, 0);
                CursorPosition += tag.Length;
            }
            else
            {
                textBox1.Select(CursorPosition + tag.Length / 2, 0);
                CursorPosition += tag.Length / 2;
            }
        }

        private void textBox1_MouseUp(object sender, MouseEventArgs e)
        {
            CursorPosition = textBox1.SelectionStart;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            CursorPosition = textBox1.SelectionStart;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            EnterTag("<b></b>");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            EnterTag("<i></i>");
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.webBrowser1.Navigate("about:blank");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    try
                    {
                        sw.Write(webBrowser1.DocumentText);
                    }
                    catch
                    {
                        MessageBox.Show("File cannot be saved: " +
                        saveFileDialog1.FileName);
                    }
            }
        }


        private void SetText(string tekst)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.listBox1.Items.Add(tekst);
            }
        }
        private void SetTextHTML(string tekst)
        {
            if (webBrowser1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetTextHTML);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.webBrowser1.Document.Write(tekst);
            }
        }
        private void SetScroll()
        {
            if (webBrowser1.InvokeRequired)
            {
                SetScrollCallBack s = new SetScrollCallBack(SetScroll);
                this.Invoke(s);
            }
            else
            {
                this.webBrowser1.Document.Window.ScrollTo(1, Int32.MaxValue);
            }
        }

        private void WriteText(string who, string message)
        {
            SetTextHTML("<table><tr><td width=\"10%\"><b>" + who + "</b></td><td width=\"90%\">(" + DateTime.Now.ToShortTimeString() + "):</td></tr>");
            SetTextHTML("<tr><td colspan=2>" + message + "</td></tr></table>");
            SetTextHTML("<hr>");
            SetScroll();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            adresIP = comboBox1.Text;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            IPAddress serwerIP;
            try
            {
                serwerIP = IPAddress.Parse(adresIP);
            }
            catch
            {
                MessageBox.Show("Błędny adres IP");
                isConnectionAlive = false;
                return;
            }
            server = new TcpListener(serwerIP, (int)numericUpDown1.Value);
            try
            {
                server.Start();
                SetText("Waiting for connection ...");
                client = server.AcceptTcpClient();
                NetworkStream ns = client.GetStream();
                SetText("Client is trying to connect");
                reading = new BinaryReader(ns);
                writing = new BinaryWriter(ns);
                if (reading.ReadString() == "###HI###")
                {
                    SetText("Client is connected.");
                    backgroundWorker2.RunWorkerAsync();
                }
                else
                {
                    SetText("Client authorization failure! Connection aborted.");
                    client.Close();
                    server.Stop();
                    isConnectionAlive = false;
                }
            }
            catch
            {
                SetText("Connection was closed/aborted.");
                isConnectionAlive = false;
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            string message;
            try
            {
                while ((message = reading.ReadString()) != "###BYE###")
                {
                    WriteText("Someone", message);
                }
                client.Close();
                server.Stop();
                SetText("Connection was closed by client.");
            }
            catch
            {
                SetText("Client dissconnected.");
                isConnectionAlive = false;
                client.Close();
                server.Stop();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isConnectionAlive == false)
            {
                isConnectionAlive = true;
                backgroundWorker1.RunWorkerAsync();
            }
            else
            {
                isConnectionAlive = false;
                if (client != null)
                    client.Close();
                server.Stop();
                backgroundWorker1.CancelAsync();
                if (backgroundWorker2.IsBusy)
                    backgroundWorker2.CancelAsync();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WriteText("Me", textBox1.Text);
            try
            {
                if (isConnectionAlive)
                    writing.Write(textBox1.Text);
            }
            catch {
                textBox1.Text = "No client connected";
            }
            textBox1.Text = "";
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                this.button2_Click(sender, e);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    try
                    {
                        sw.Write(webBrowser1.DocumentText);
                    }
                    catch
                    {
                        MessageBox.Show("File cannot be saved: " + saveFileDialog1.FileName);
                    }
            }
        }

        private void clearToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.webBrowser1.Navigate("about:blank");
        }
    }
}
