﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace TCP_Server_Connection
{
    public partial class Form1 : Form
    {
        private TcpListener server;
        private TcpClient client;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPAddress adresIP;

            try
            {
                adresIP = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Wrong IP address format!", "Error");
                textBox1.Text = String.Empty;
                return;
            }

            int port = System.Convert.ToInt16(numericUpDown1.Value);

            try
            {
                server = new TcpListener(adresIP, port);
                server.Start();
                client = server.AcceptTcpClient();
                IPEndPoint IP = (IPEndPoint)client.Client.RemoteEndPoint;
                listBox1.Items.Add("[" + IP.ToString() + "] :Connection Established");
                listBox1.Items.Add("Connection Established!");
                button1.Enabled = false;
                button2.Enabled = true;
                client.Close();
                server.Stop();
            }
            catch (Exception ex)
            {
                listBox1.Items.Add("Server initialization error!");
                MessageBox.Show(ex.ToString(), "Error");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            server.Stop();
            client.Close();
            listBox1.Items.Add("Server was stopped ...");
            button1.Enabled = true;
            button2.Enabled = false;
        }
    }
}