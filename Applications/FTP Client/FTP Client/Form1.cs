﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
namespace FTP_Client
{
    public partial class Form1 : Form
    {
        private FTPClient client = new FTPClient();

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                textBoxLocalPath.Text = folderBrowserDialog1.SelectedPath;
        }

        private void GetFtpContent(ArrayList directoriesList)
        {
            listBoxFtpDir.Items.Clear();
            listBoxFtpDir.Items.Add("[..]");
            directoriesList.Sort();
            foreach (string name in directoriesList)
            {
                string position = name.Substring(name.LastIndexOf(' ') + 1, name.Length - name.LastIndexOf(' ') - 1);
                if (position != ".." && position != ".")
                    switch (name[0])
                    {
                        case 'd':
                            listBoxFtpDir.Items.Add("[" + position + "]");
                            break;
                        case 'l':
                            listBoxFtpDir.Items.Add("->" + position);
                            break;
                        default:
                            listBoxFtpDir.Items.Add(position);
                            break;
                    }
            }
        }

        void client_DownCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
                MessageBox.Show("Error: " + e.Error.Message);
            else
                MessageBox.Show("File Downloaded");
            client.DownloadCompleted = true;
            buttonDownload.Enabled = true;
            buttonUpload.Enabled = true;
        }

        void client_DownProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            toolStripStatusLabelDownload.Text = "Downloaded: " + (e.BytesReceived /
            (double)1024).ToString() + " kB";
        }

        void client_UpCompleted(object sender, System.Net.UploadFileCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error.Message);
                client.UploadCompleted = true;
                buttonUpload.Enabled = true;
                buttonDownload.Enabled = true;
                return;
            }
            client.UploadCompleted = true;
            buttonUpload.Enabled = true;
            buttonDownload.Enabled = true;
            MessageBox.Show("File Uploaded");
            try
            {
                GetFtpContent(client.GetDirectories());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        void client_UpProgressChanged(object sender, System.Net.UploadProgressChangedEventArgs e)
        {
            toolStripStatusLabelDownload.Text = "Sent: " + (e.BytesSent /
            (double)1024).ToString() + " kB";
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (comboBoxServer.Text != string.Empty & comboBoxServer.Text.Trim() !=
            String.Empty)
                try
                {
                    string serverName = comboBoxServer.Text;
                    //nazwa hosta nie może zaczynaćsięod ftp://
                    if (serverName.StartsWith("ftp://"))
                        serverName = serverName.Replace("ftp://", "");
                    client = new FTPClient(serverName, textBoxLogin.Text, maskedTextBoxPass.Text);
                    //
                    client.DownProgressChanged += new FTPClient.DownProgressChangedEventHandler(client_DownProgressChanged);
                    client.DownCompleted += new FTPClient.DownCompletedEventHandler(client_DownCompleted);
                    //
                    //
                    client.UpCompleted += new FTPClient.UpCompletedEventHandler(client_UpCompleted);
                    client.UpProgressChanged += new FTPClient.UpProgressChangedEventHandler(client_UpProgressChanged);
                    //
                    GetFtpContent(client.GetDirectories());
                    textBoxFtpPath.Text = client.FtpDirectory;
                    toolStripStatusLabelServer.Text = "Serwer: ftp://" + client.Host;
                    buttonConnect.Enabled = false;
                    buttonDisconnect.Enabled = true;
                    buttonDownload.Enabled = true;
                    buttonUpload.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                }
            else
            {
                MessageBox.Show("Wprowadźnazwęserwera FTP", "Błąd");
                comboBoxServer.Text = string.Empty;
            }
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            client.DownProgressChanged -= new FTPClient.DownProgressChangedEventHandler(client_DownProgressChanged);
            client.DownCompleted -= new FTPClient.DownCompletedEventHandler(client_DownCompleted);
            client.UpCompleted -= new FTPClient.UpCompletedEventHandler(client_UpCompleted);
            client.UpProgressChanged -= new FTPClient.UpProgressChangedEventHandler(client_UpProgressChanged);
            buttonConnect.Enabled = true;
            listBoxFtpDir.Items.Clear();
            textBoxFtpPath.Text = "";
        }

        private void buttonDownload_Click(object sender, EventArgs e)
        {
            int index = listBoxFtpDir.SelectedIndex;
            if (listBoxFtpDir.Items[index].ToString()[0] != '[')
            {
                if (MessageBox.Show("Download File?", "Downloading File",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question) ==
                DialogResult.OK)
                {
                    try
                    {
                        string localFile = textBoxLocalPath.Text + "\\" +
                        listBoxFtpDir.Items[index].ToString();
                        FileInfo fi = new FileInfo(localFile);
                        if (fi.Exists == false)
                        {
                            client.DownloadFileAsync(listBoxFtpDir.Items[index].ToString(), localFile);
                            buttonDownload.Enabled = false;
                            buttonUpload.Enabled = false;
                        }
                        else
                            MessageBox.Show("File Exists ");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
        }

        private void buttonUpDir_Click(object sender, EventArgs e)
        {
            //client.ChangeDirectoryUp();
        }

        private void listBoxFtpDir_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxFtpDir.SelectedIndex;
            try
            {
                if (index > -1)
                {
                    if (index == 0)
                        GetFtpContent(client.ChangeDirectoryUp());
                    else
                        if (listBoxFtpDir.Items[index].ToString()[0] == '[')
                        {
                            string directory =
                            listBoxFtpDir.Items[index].ToString().Substring(1,
                            listBoxFtpDir.Items[index].ToString().Length - 2);
                            GetFtpContent(client.ChangeDirectory(directory));
                        }
                        else if (listBoxFtpDir.Items[index].ToString()[0] == '-' &
                        listBoxFtpDir.Items[index].ToString()[2] == '.')
                        {
                            string link =
                            listBoxFtpDir.Items[index].ToString().Substring(5,
                            listBoxFtpDir.Items[index].ToString().Length - 5);
                            client.FtpDirectory = "ftp://" + client.Host;
                            GetFtpContent(client.ChangeDirectory(link));
                        }
                        else
                            this.buttonUpDir_Click(sender, e);
                    listBoxFtpDir.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }


        private void listBoxFtpDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.listBoxFtpDir_MouseDoubleClick(sender, null);
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    client.UploadFileAsync(openFileDialog1.FileName);
                    buttonDownload.Enabled = false;
                    buttonUpload.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                int indeks = listBoxFtpDir.SelectedIndex;
                if (indeks > -1)
                    if (listBoxFtpDir.Items[indeks].ToString()[0] != '[')
                    {
                        try
                        {
                            MessageBox.Show(client.DeleteFile(listBoxFtpDir.Items[indeks].ToString()));
                            GetFtpContent(client.GetDirectories());
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("File cannot be deleted" +
                            " (" + ex.Message + ")");
                        }

                    }
            }
        }
    }
}

