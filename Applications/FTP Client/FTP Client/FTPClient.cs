﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Net;
using System.IO;
using System.Net.NetworkInformation;

namespace FTP_Client
{
    class FTPClient
    {
        public FTPClient()
        {
            downloadCompleted = true;
            uploadCompleted = true;
        }

        public FTPClient(string host, string userName, string password)
        {
            this.host = host;
            this.username = userName;
            this.password = password;
            ftpDirectory = "ftp://" + this.host;
        }

        #region Pola
        private string host;
        private string username;
        private string password;

        private string ftpDirectory;
        private bool downloadCompleted;
        private bool uploadCompleted;
        #endregion

        #region Własności
        public string Host
        {
            get
            {
                return host;
            }
            set
            {
                host = value;
            }
        }

        public string UserName
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }
        public string FtpDirectory
        {
            get
            {
                if (ftpDirectory.StartsWith("ftp://"))
                    return ftpDirectory;
                else
                    return "ftp://" + ftpDirectory;
            }
            set
            {
                ftpDirectory = value;
            }
        }
        public bool DownloadCompleted
        {
            get
            {
                return downloadCompleted;
            }
            set
            {
                downloadCompleted = value;
            }
        }
        public bool UploadCompleted
        {
            get
            {
                return uploadCompleted;
            }
            set
            {
                uploadCompleted = value;
            }
        }
        #endregion

        //s1
        public ArrayList GetDirectories()
        {
            ArrayList directories = new ArrayList();
            FtpWebRequest request;
            try
            {
                request = (FtpWebRequest)WebRequest.Create(ftpDirectory);
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                request.Credentials = new NetworkCredential(this.username, this.password);
                request.KeepAlive = false;
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Stream stream = response.GetResponseStream();
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string directory;
                        while ((directory = reader.ReadLine()) != null)
                        {
                            directories.Add(directory);
                        }
                    }
                }
                return directories;
            }
            catch
            {
                throw new Exception("Błąd: Nie można nawiązaćpołączenia z " + host);
            }
        }
        //es1

        //s2
        public ArrayList ChangeDirectory(string DirectoryName)
        {
            ftpDirectory += "/" + DirectoryName;
            return GetDirectories();
        }
        //es2

        //s3
        public ArrayList ChangeDirectoryUp()
        {
            if (ftpDirectory != "ftp://" + host)
            {
                ftpDirectory = ftpDirectory.Remove(ftpDirectory.LastIndexOf("/"), ftpDirectory.Length - ftpDirectory.LastIndexOf("/"));
                return GetDirectories();
            }
            else
                return GetDirectories();
        }
        //es3

        //s4
        public void DownloadFileAsync(string ftpFileName, string localFileName)
        {
            WebClient client = new WebClient();
            try
            {
                Uri uri = new Uri(ftpDirectory + "/" + ftpFileName);
                FileInfo file = new FileInfo(localFileName);
                if (file.Exists)
                    throw new Exception("Error: File " + localFileName + " exists");
                else
                {
                    client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(client_DownloadFileCompleted);
                    client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                    client.Credentials = new NetworkCredential(this.username,
                    this.password);
                    client.DownloadFileAsync(uri, localFileName);
                    downloadCompleted = false;
                }
            }
            catch
            {
                client.Dispose();
                throw new Exception("Error: File cannot be downloaded");
            }
        }
        //es4

        //s5
        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //throw new NotImplementedException();
            this.OnDownloadProgressChanged(sender, e);
        }

        void client_DownloadFileCompleted(object sender,
        System.ComponentModel.AsyncCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            this.OnDownloadCompleted(sender, e);
        }
        //es5

        //s6
        public delegate void DownProgressChangedEventHandler(object sender, DownloadProgressChangedEventArgs e);
        public event DownProgressChangedEventHandler DownProgressChanged;
        protected virtual void OnDownloadProgressChanged(object sender,
        DownloadProgressChangedEventArgs e)
        {
            if (DownProgressChanged != null) DownProgressChanged(sender, e);
        }
        //es6

        //s7
        public delegate void DownCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
        public event DownCompletedEventHandler DownCompleted;
        protected virtual void OnDownloadCompleted(object sender,
        System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (DownCompleted != null) DownCompleted(sender, e);
        }
        //es7


        //s8a
        void client_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            //throw new NotImplementedException();
            this.OnUploadProgressChanged(sender, e);
        }
        void client_UploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            this.OnUploadCompleted(sender, e);
        }

        //es8a

        //s8b
        public delegate void UpCompletedEventHandler(object sender, UploadFileCompletedEventArgs e);
        public event UpCompletedEventHandler UpCompleted;
        protected virtual void OnUploadCompleted(object sender,
        UploadFileCompletedEventArgs e)
        {
            if (UpCompleted != null) UpCompleted(sender, e);
        }
        //es8b

        //s8
        public void UploadFileAsync(string FileName)
        {
            try
            {
                System.Net.Cache.RequestCachePolicy cache = new System.Net.Cache.
                RequestCachePolicy(System.Net.Cache.RequestCacheLevel.Reload);
                WebClient client = new WebClient();
                FileInfo file = new FileInfo(FileName);
                Uri uri = new Uri((FtpDirectory + '/' + file.Name).ToString());
                client.Credentials = new NetworkCredential(this.username,
                this.password);
                uploadCompleted = false;
                if (file.Exists)
                {
                    client.UploadFileCompleted += new UploadFileCompletedEventHandler(client_UploadFileCompleted);
                    client.UploadProgressChanged += new UploadProgressChangedEventHandler(client_UploadProgressChanged);
                    client.UploadFileAsync(uri, FileName);
                }
            }
            catch
            {
                throw new Exception("Error: File cannot be uploaded.");
            }
        }
        //es8

        //s9
        public delegate void UpProgressChangedEventHandler(object sender, UploadProgressChangedEventArgs e);
        public event UpProgressChangedEventHandler UpProgressChanged;
        protected virtual void OnUploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            if (UpProgressChanged != null) UpProgressChanged(sender, e);
        }
        //es9

        //s10
        public string DeleteFile(string nazwa)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create
                (ftpDirectory + "//" + nazwa);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = new NetworkCredential(this.username,
                this.password);
                request.KeepAlive = false;
                request.UsePassive = true;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                return response.StatusDescription;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: File cannot be deleted " + nazwa +
                " (" + ex.Message + ")");
            }
        }
        //es10
    }


}

