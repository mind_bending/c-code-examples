﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;

namespace TCPClientConnectionApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string host = txtHostAddress.Text;
            int port = System.Convert.ToInt16(numericPort.Value);
                try
                {
                    TcpClient Client = new TcpClient(host, port);
                    listBox1.Items.Add("Established connection with: " + host + " on port: " + port);
                    Client.Close();
                }
                catch(Exception ex)
                {
                    listBox1.Items.Add("Error: Connection was not established!");
                    MessageBox.Show(ex.ToString());
                }
         }
    }
}
