﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicTacToe_With_AI
{
    public partial class Form1 : Form
    {
        private bool turn = true; // true = X turn; flase = Y turn;
        private bool againstAI = false;
        int turnCount = 0;
        static String player1, player2;

        public Form1()
        {
            InitializeComponent();
        }

        public static void setPlayerNames(String name1, String name2){

            player1 = name1;
            player2 = name2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            User form = new User();
            form.ShowDialog();
            label1.Text = player1;
            label2.Text = player2;
            if (label2.Text.Trim() == "COMPUTER")
            {
                againstAI = true;
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Artur Wieczorek", "Tic Tac Toe");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                b.Text = "X";
            }
            else {
                b.Text = "O";
            }

            turn = !turn;
            b.Enabled = false;
            turnCount ++;
            checkForWinner();

            if ((!turn) && (againstAI)) {
                computerMakeMove();
            }
        }

        private void checkForWinner() {
            bool thereIsAWinner = false;

            // Horizontal Checking
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled)) {
                thereIsAWinner = true;
            }

            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
            {
                thereIsAWinner = true;
            }

            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
            {
                thereIsAWinner = true;
            }

            // Vertical Checking
            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
            {
                thereIsAWinner = true;
            }

            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
            {
                thereIsAWinner = true;
            }

            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
            {
                thereIsAWinner = true;
            }

            // Diagonal Checking
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            {
                thereIsAWinner = true;
            }

            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
            {
                thereIsAWinner = true;
            }

            if (thereIsAWinner)
            {

                disableButtons();

                String winner = "";
                if (turn)
                {
                    winner = player2;
                    oWinCount.Text = (Int16.Parse(oWinCount.Text) + 1).ToString();
                    disableButtons();
                }
                else
                {
                    winner = player1;
                    xWinCount.Text = (Int16.Parse(xWinCount.Text) + 1).ToString();
                    disableButtons();
                }
                MessageBox.Show("Winner: " + winner, "You Win!");
                
            }
            else {
                if (turnCount == 9) {
                    drawCount.Text = (Int16.Parse(drawCount.Text) + 1).ToString();
                    MessageBox.Show("There is a draw.","Draw!");  
                }
            }
        }

        private void computerMakeMove() {
            Button move = null;

            move = lookForWinOrBlock("O");
            
            if (move == null) 
            {
                move = lookForWinOrBlock("X");

                if (move == null)
                {
                    move = lookForCorner();

                    if (move == null)
                    {
                        move = lookForOpenSpace();
                    }
                }
            }
           
            if( move!=null )
                move.PerformClick();
        }

        private Button lookForWinOrBlock(String mark){

            Console.WriteLine("Looking for win or block: " + mark);
            //Horizonal Tests:
            //A
            if ((A1.Text == mark) && (A2.Text == mark) && (A3.Text == ""))
                return A3;
            if ((A2.Text == mark) && (A3.Text == mark) && (A1.Text == ""))
                return A1;
            if ((A1.Text == mark) && (A3.Text == mark) && (A2.Text == ""))
                return A2;
            //B
            if ((B1.Text == mark) && (B2.Text == mark) && (B3.Text == ""))
                return B3;
            if ((B2.Text == mark) && (B3.Text == mark) && (B1.Text == ""))
                return B1;
            if ((B1.Text == mark) && (B3.Text == mark) && (B2.Text == ""))
                return B2;

            //C
            if ((C1.Text == mark) && (C2.Text == mark) && (C3.Text == ""))
                return C3;
            if ((C2.Text == mark) && (C3.Text == mark) && (C1.Text == ""))
                return C1;
            if ((C1.Text == mark) && (C3.Text == mark) && (C2.Text == ""))
                return C2;

            //Vertical Tests:
            if ((A1.Text == mark) && (B1.Text == mark) && (C1.Text == ""))
                return C1;
            if ((B1.Text == mark) && (C1.Text == mark) && (A1.Text == ""))
                return A1;
            if ((A1.Text == mark) && (C1.Text == mark) && (B1.Text == ""))
                return B1;

            //
            if ((A2.Text == mark) && (B2.Text == mark) && (C2.Text == ""))
                return C2;
            if ((B2.Text == mark) && (C2.Text == mark) && (A2.Text == ""))
                return A2;
            if ((A2.Text == mark) && (C2.Text == mark) && (B2.Text == ""))
                return B2;

            //
            if ((A3.Text == mark) && (B3.Text == mark) && (C3.Text == ""))
                return C3;
            if ((B3.Text == mark) && (C3.Text == mark) && (A3.Text == ""))
                return A3;
            if ((A3.Text == mark) && (C3.Text == mark) && (B3.Text == ""))
                return B3;

            //Diagonal Tests:
            if ((A1.Text == mark) && (B2.Text == mark) && (C3.Text == ""))
                return C3;
            if ((B2.Text == mark) && (C3.Text == mark) && (A1.Text == ""))
                return A1;
            if ((A1.Text == mark) && (C3.Text == mark) && (B2.Text == ""))
                return B2;


            if ((A3.Text == mark) && (B2.Text == mark) && (C1.Text == ""))
                return C1;
            if ((B2.Text == mark) && (C1.Text == mark) && (A3.Text == ""))
                return A3;
            if ((A3.Text == mark) && (C1.Text == mark) && (B2.Text == ""))
                return B2;

            return null;
        }

        private Button lookForCorner() {
            
            Console.WriteLine("Looking for corner");
            //1
            if (A1.Text == "O")
            {
                if (A3.Text == "")
                    return A3;
                if (C3.Text == "")
                    return C3;
                if (C1.Text == "")
                    return C1;
            }

            //2
            if (A3.Text == "O")
            {
                if (A1.Text == "")
                    return A1;
                if (C3.Text == "")
                    return C3;
                if (C1.Text == "")
                    return C1;
            }

            //3
            if (C3.Text == "O")
            {
                if (A1.Text == "")
                    return A3;
                if (A3.Text == "")
                    return A3;
                if (C1.Text == "")
                    return C1;
            }

            //4
            if (C1.Text == "O")
            {
                if (A1.Text == "")
                    return A3;
                if (A3.Text == "")
                    return A3;
                if (C3.Text == "")
                    return C3;
            }

            //5
            if (A1.Text == "O")
                return A1;
            if (A3.Text == "")
                return A3;
            if (C1.Text == "")
                return C1;
            if (C3.Text == "")
                return C1;
            
            return null;
        }
        
        private Button lookForOpenSpace() {
            
            Console.WriteLine("Looking for open space");
            Button b = null;

            foreach (Control c in Controls){
                b = c as Button;
                if (b != null)
                {
                    if (b.Text == "")
                        return b;
                }
            }

            return null;
        }

        private void disableButtons() {

                foreach (Control c in Controls)
                {
                    try
                    {
                        Button b = (Button)c;
                        b.Enabled = false;
                    }
                    catch { }
                }    
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turnCount = 0;

            

                foreach (Control c in Controls)
                {
                    try
                    {
                        Button b = (Button)c;
                        b.Enabled = true;
                        b.Text = "";

                    }catch { };
                }
        }

        private void buttonEnter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                {
                    b.Text = "X";
                }
                else
                {
                    b.Text = "O";
                }
            }
        }

        private void buttonLeave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void resetWinCountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            oWinCount.Text = "0";
            xWinCount.Text = "0";
            drawCount.Text = "0";
        }
    }
}
