﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;

namespace Ping_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != String.Empty)
                if (textBox2.Text.Trim().Length > 0)
                {
                    listBox2.Items.Add(textBox2.Text);
                    textBox1.Clear();
                }
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
        }

        private string SendPing(string adres, int timeout, byte[] bufor, PingOptions opcje)
        {
            Ping ping = new Ping();
            try
            {
                PingReply reply = ping.Send(adres, timeout, bufor, opcje);
                if (reply.Status == IPStatus.Success)
                    return "Response: " + adres + " bytes=" +
                    reply.Buffer.Length + " time=" + reply.RoundtripTime +
                    "ms TTL=" + reply.Options.Ttl;
                else
                    return "Error:" + adres + " " + reply.Status.ToString();
            }
            catch (Exception ex)
            {
                return "Error:" + adres + " " + ex.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (textBox1.Text != "" || listBox2.Items.Count > 0)
            {
                PingOptions options = new PingOptions();
                options.Ttl = (int)numericUpDown2.Value;
                options.DontFragment = true;
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

                byte[] bufor = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                if (textBox1.Text != "")
                {
                    for (int i = 0; i < (int)numericUpDown1.Value; i++)
                        listBox1.Items.Add(this.SendPing(textBox1.Text, timeout,
                        bufor, options));
                    listBox1.Items.Add("----------------");
                }

                if (listBox2.Items.Count > 0)
                {
                    foreach (string host in listBox2.Items)
                    {
                        for (int i = 0; i < (int)numericUpDown1.Value; i++)
                            listBox1.Items.Add(this.SendPing(host, timeout,
                            bufor, options));
                        listBox1.Items.Add("----------------");
                    }
                }
            }
            else
            {
                MessageBox.Show("User didn't add an hosts", "Error");
            }
        }
        //End of Button1_Click

        // Those are Asynchronous methods can can be used in place of SendPing method;
        // just change:
        // listBox1.Items.Add(this.SendPing(textBox1.Text, timeout,bufor, options));
        // to:
        //for (int i = 0; i < (int)numericUpDown1.Value; i++)
        //   SendAsynchronousPing(textBox1.Text, timeout,bufor, options));
        // comment by Artur Wieczorek
                

        public void SendAsynchronousPing(string address, int timeout, byte[] buffer, PingOptions options)
        {
            Ping ping = new Ping();
            ping.PingCompleted += new PingCompletedEventHandler(EndPing);
            try
            {
                ping.SendAsync(address, timeout, buffer, options, null);
            }
            catch (Exception ex)
            {
                listBox1.Items.Add("Error: " + address + " " + ex.Message);
            }
        }
        //End of SendAsynchronousPing

        public void EndPing(object sender, PingCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                listBox1.Items.Add("Error: Operation Aborted or Incorrect Address ");
                ((IDisposable)(Ping)sender).Dispose();
                return;
            }
            PingReply response = e.Reply;
            if (response.Status == IPStatus.Success)
            {
                listBox1.Items.Add("Response " + response.Address.ToString() + "bytes=" + response.Buffer.Length + " time=" +
                response.RoundtripTime + "ms TTL=" + response.Options.Ttl);
            }                      
            else
            {
                listBox1.Items.Add("Error: No response from " + e.Reply.Address + ":" + response.Status);
                ((IDisposable)(Ping)sender).Dispose();
            }
        }
        //End of EndPing
    }
}
