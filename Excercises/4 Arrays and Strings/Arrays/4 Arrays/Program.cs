﻿// Compute the average of a set of values.
using System;
class Average
{
    static void Main()
    {
        int[] nums = { 99, 10, 100, 18, 78, 23, 63, 9, 87, 49 };
        int avg = 0;
        for (int i = 0; i < 10; i++)
            avg = avg + nums[i];
        avg = avg / 10;
        Console.WriteLine("Average: " + avg);

        //===== 2 dimensional array: ========

        int t, j;
        int[,] table = new int[3, 4];
        for (t = 0; t < 3; ++t)
        {
            for (j = 0; j < 4; ++j)
            {
                table[t, j] = (t * 4) + j + 1;
                Console.Write(table[t, j] + " ");
            }



        } Console.ReadLine();
    }
}
