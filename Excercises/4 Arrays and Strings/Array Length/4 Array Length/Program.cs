﻿// Reverse an array.
using System;
class RevCopy
{
    static void Main()
    {
        int i, j;
        int[] nums1 = new int[10];
        int[] nums2 = new int[10];
        for (i = 0; i < nums1.Length; i++) nums1[i] = i;
        Console.Write("Original contents: ");
        for (i = 0; i < nums2.Length; i++)
            Console.Write(nums1[i] + " ");
        Console.WriteLine();
        // Reverse copy nums1 to nums2.
        if (nums2.Length >= nums1.Length) // make sure nums2 is long enough
            for (i = 0, j = nums1.Length - 1; i < nums1.Length; i++, j--)
                nums2[j] = nums1[i];
        Console.Write("Reversed contents: ");
        for (i = 0; i < nums2.Length; i++)
            Console.Write(nums2[i] + " ");
        Console.WriteLine();

        //Another example with jagged arrays

        int[][] network_nodes = new int[4][];
        network_nodes[0] = new int[3];
        network_nodes[1] = new int[7];
        network_nodes[2] = new int[2];
        network_nodes[3] = new int[5];
        int k, l;
        // Fabricate some fake CPU usage data.
        for (k = 0; k < network_nodes.Length; k++)
            for (l = 0; l< network_nodes[k].Length; l++)
                network_nodes[k][l] = k * l + 70;
        Console.WriteLine("Total number of network nodes: " + network_nodes.Length + "\n");
        for (k = 0; k < network_nodes.Length; k++)
        {
            for (l = 0; l < network_nodes[k].Length; l++)
            {
                Console.Write("CPU usage at node " + k + " CPU " + l + ": ");
                Console.Write(network_nodes[k][l] + "% ");
                Console.WriteLine();
            }
            Console.WriteLine();

        }
        Console.ReadLine();
    }
}
