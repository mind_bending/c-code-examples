﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {

            string strA = "C# strings are powerful.";
            char[] charray = {'t', 'e', 's', 't'};
            string strB = new string(charray);

            Console.WriteLine(strA);
            Console.WriteLine(strB);

            string str1 = "When it comes to .NET programming, C# is #1.";
            string str2 = "When it comes to .NET programming, C# is #1.";
            string str3 = "C# strings are powerful.";
            string strUp, strLow;
            int result, idx;
            Console.WriteLine("str1: " + str1);
            Console.WriteLine("Length of str1: " + str1.Length);
            // Create upper- and lowercase versions of str1.
            strLow = str1.ToLower(CultureInfo.CurrentCulture);
            strUp = str1.ToUpper(CultureInfo.CurrentCulture);
            Console.WriteLine("Lowercase version of str1:\n " +
            strLow);
            Console.WriteLine("Uppercase version of str1:\n " +
            strUp);
            Console.WriteLine();
            // Display str1, one char at a time.
            Console.WriteLine("Display str1, one char at a time.");
            for (int i = 0; i < str1.Length; i++)
                Console.Write(str1[i]);
            Console.WriteLine("\n");
            // Compare strings using == and !=. These comparisons are ordinal.
            if (str1 == str2)
                Console.WriteLine("str1 == str2");
            else
                Console.WriteLine("str1 != str2");
            if (str1 == str3)
                Console.WriteLine("str1 == str3");
            else
                Console.WriteLine("str1 != str3");
            // This comparison is culture-sensitive.
            result = string.Compare(str1, str3, StringComparison.CurrentCulture);
            if (result == 0)
                Console.WriteLine("str1 and str3 are equal");
            else if (result < 0)
                Console.WriteLine("str1 is less than str3");
            else
                Console.WriteLine("str1 is greater than str3");
            Console.WriteLine();
            // Assign a new string to str2.
            str2 = "One Two Three One";
            // Search a string.
            idx = str2.IndexOf("One", StringComparison.Ordinal);
            Console.WriteLine("Index of first occurrence of One: " + idx);
            idx = str2.LastIndexOf("One", StringComparison.Ordinal);
            Console.WriteLine("Index of last occurrence of One: " + idx);

            //Another example

            int num;
            int nextdigit;
            int numdigits;
            int[] n = new int[20];
            string[] digits = { "zero", "one", "two","three", "four", "five", "six", "seven", "eight","nine" };
            num = 1908;
            Console.WriteLine("Number: " + num);
            Console.Write("Number in words: ");
            nextdigit = 0;
            numdigits = 0;
            // Get individual digits and store in n.
            // These digits are stored in reverse order.
            do
            {
                nextdigit = num % 10;
                n[numdigits] = nextdigit;
                numdigits++;
                num = num / 10;
            } while (num > 0);
            numdigits--;
            // Display the words.
            for (; numdigits >= 0; numdigits--)
                Console.Write(digits[n[numdigits]] + " ");
            Console.WriteLine();
            string orgstr = "C# makes strings easy.";
            // Construct a substring.
            string substr = orgstr.Substring(5, 12);
            Console.WriteLine("orgstr: " + orgstr);
            Console.WriteLine("substr: " + substr);

            Console.ReadLine();
        }
    }
}
