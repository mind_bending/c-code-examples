﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interface_simple_example
{
    interface ISeries
    {
        int GetNext(); // return next number in series
        void Reset(); // restart
        void SetStart(int x); // set starting value
    }
}
