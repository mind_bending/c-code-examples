﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interface_simple_example
{
    class Program
    {
        static void Main(string[] args)
        {
            ByTwos ob = new ByTwos();
            for (int i = 0; i < 5; i++)
                Console.WriteLine("Next value is " +
                ob.GetNext());
            Console.WriteLine("\nResetting");
            ob.Reset();
            for (int i = 0; i < 5; i++)
                Console.WriteLine("Next value is " +
                ob.GetNext());
            Console.WriteLine("\nStarting at 100");
            ob.SetStart(100);
            for (int i = 0; i < 5; i++)
                Console.WriteLine("Next value is " +
                ob.GetNext());
            Console.ReadLine();
        }
    }
}
