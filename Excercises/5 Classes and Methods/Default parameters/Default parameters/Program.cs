﻿// Demonstrate optional arguments.
using System;
class OptionArgDemo
{
    static void OptArgMeth(int alpha, int beta = 10, int gamma = 20)
    {
        Console.WriteLine("Here is alpha, beta, and gamma: " +
        alpha + " " + beta + " " + gamma);
    }

    // Display part or all of string.
    static void Display(string str, int start = 0, int stop = -1)
    {
        if (stop < 0)
            stop = str.Length;
        // Check for out-of-range condition.
        if (stop > str.Length | start > stop | start < 0)
            return;
        for (int i = start; i < stop; i++)
            Console.Write(str[i]);
        Console.WriteLine();
    }

    static void Main()
    {
        // Pass all arguments explicitly.
        OptArgMeth(1, 2, 3);
        // Let gamma default.
        OptArgMeth(1, 2);
        // Let both beta and gamma default.
        OptArgMeth(1);

        Display("this is a test");
        Display("this is a test", 10);
        Display("this is a test", 5, 12);

        Console.ReadLine();
    }
}
