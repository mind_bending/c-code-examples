﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2_For_loop
{
    class Program
    {
        static void Main(string[] args)
        {

            // A negatively running for loop.
        int x;
        for(x = 100; x > -100; x -= 20)
            Console.WriteLine(x);

        int k, j;
        for(k=0, j=10; k < j; k++, j--)
            Console.WriteLine("k and j: " + k + " " + j);

        Console.ReadLine();
        }
    }

}
    

