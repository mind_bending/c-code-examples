﻿// Demonstrate the switch.
using System;
class SwitchDemo
{
    static void Main()
    {
        int i;
        for (i = 0; i < 10; i++)
            switch (i)
            {
                case 0:
                    Console.WriteLine("i is zero");
                    break;
                case 1:
                    Console.WriteLine("i is one");
                    break;
                case 2:
                    Console.WriteLine("i is two");
                    break;
                case 3:
                    Console.WriteLine("i is three");
                    break;
                case 4:
                    Console.WriteLine("i is four");
                    break;
                default:
                    Console.WriteLine("i is five or more");
                    break;
            }

                    // Use a char to control the switch.

                    char ch;
                    for (ch = 'A'; ch <= 'E'; ch++)
                        switch (ch)
                        {
                            case 'A':
                                Console.WriteLine("ch is A");
                                break;
                            case 'B':
                                Console.WriteLine("ch is B");
                                break;
                            case 'C':
                                Console.WriteLine("ch is C");
                                break;
                            case 'D':
                                Console.WriteLine("ch is D");
                                break;
                            case 'E':
                                Console.WriteLine("ch is E");
                                break;
                                
                        }Console.ReadLine();
            } 
    } 

