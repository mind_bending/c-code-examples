﻿// Using break to exit a loop.
using System;
class BreakDemo
{
    static void Main()
    {
        // Use break to exit this loop.
        for (int i = -10; i <= 10; i++)
        {
            if (i > 0) break; // terminate loop when i is positive
            Console.Write(i + " ");
        }
        Console.WriteLine("Done");
      

        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine("Outer loop count: " + i);
            Console.Write(" Inner loop count: ");
            int t = 0;
            while (t < 100)
            {
                if (t == 10) break; // terminate loop if t is 10
                Console.Write(t + " ");
                t++;
            }
            Console.WriteLine();
        }
        Console.WriteLine("Loops complete.");

        // Use continue.

        // Print even numbers between 0 and 100.
        for(int i = 0; i <= 20; i++) {
            if((i%2) != 0) continue; // iterate
            Console.WriteLine(i);

        }
        Console.ReadLine();
    }
}
