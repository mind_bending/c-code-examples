﻿// Add a method to Building.
using System;

class Building {

    public int Floors; // number of floors
    public int Area; // total square footage of building
    public int Occupants; // number of occupants

    // Display the area per person.
    public int AreaPerPerson() {  
    return Area / Occupants;
    }
}

// Use the AreaPerPerson() method.

class BuildingDemo {

    static void Main() {

    Building house = new Building();
    Building office = new Building();

    int areaPPo;
    int areaPPh;
    // Assign values to fields in house.
    house.Occupants = 4;
    house.Area = 2500;
    house.Floors = 2;
    areaPPh = house.AreaPerPerson();
    // Assign values to fields in office.
    office.Occupants = 25;
    office.Area = 4200;
    office.Floors = 3;
    areaPPo = office.AreaPerPerson();

    Console.WriteLine("house has:\n " +
    house.Floors + " floors\n " +
    house.Occupants + " occupants\n " +
    house.Area + " total area " +
    areaPPh + " area per person");

        Console.WriteLine();
        Console.WriteLine("office has:\n " + office.Floors + " floors\n " +
        office.Occupants + " occupants\n " + office.Area + " total area " +
        areaPPo + " area per person");
     
        Console.ReadLine();
}
}