﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _1_Variables_b
{
    class FindRadius
    {
        static void Main()
        {
            Double r;
            Double area;
            area = 10.0;
            r = Math.Sqrt(area / 3.1416);
            Console.WriteLine("Radius is " + r);

            Double theta; // angle in radians
            for (theta = 0.1; theta <= 1.0; theta = theta + 0.1)
            {
                Console.WriteLine("Sine of " + theta + " is " +
                Math.Sin(theta));
                Console.WriteLine("Cosine of " + theta + " is " +
                Math.Cos(theta));
                Console.WriteLine("Tangent of " + theta + " is " +
                Math.Tan(theta));
                Console.WriteLine();
                
            }
            Console.ReadLine();
        }


    }
}